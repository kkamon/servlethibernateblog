<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<title>Login</title>

<!-- Bootstrap -->
<link href="./vender/bootstrap-3.3.7-dist/css/bootstrap.min.css"
	rel="stylesheet">

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
<link rel="stylesheet" type="text/css" href="css/login.css">
<link rel="stylesheet" type="text/css" href="./css/default.css">
</head>
<body class="box">
	<div class="container">
		<form class="form-sign" method="post" action="RegisterServlet">
			<h1 class="text-center">Register</h1>
			<p>
				<label class="sr-only" for="firstname">First name: </label>
				<input name="firstName" type="text" placeholder="First name" class="form-control" required autofocus>
			</p>
			<p>
				<label class="sr-only" for="middle name">Middle name: </label>
				<input name="middleName" type="text" placeholder="Middle name" class="form-control" required autofocus>
			</p>
			<p>
				<label class="sr-only" for="lastname">Last name: </label>
				<input name="lastName" type="text" placeholder="Last name" class="form-control" required autofocus>
			</p>
			<p>
				<label class="sr-only" for="email">Email: </label> 
				<input name="email" type="email" placeholder="Email Address" class="form-control" required autofocus>
			</p>
			<p>
				<label class="sr-only" for="username">username: </label>
				<input name="username" type="text" placeholder="User ID" class="form-control" required autofocus>
			</p>
			<p>
				<label class="sr-only" for="email">Password: </label>
				<input name="password" type="password" placeholder="Password" class="form-control"
					required autofocus>
			</p>
			<button type="reset" class="btn btn-primary btn-block">Reset</button>
			<button type="submit" class="btn btn-primary btn-block">Sign in</button>
		</form>
	</div>

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<script src="./vender/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
</body>
</html>