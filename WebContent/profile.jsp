<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@page import="java.util.List" %>
<%@page import="com.MVCblog.dao.UserDao" %>
<%@page import="java.util.Date" %>
<%@page import="com.MVCblog.model.User" %>
<%@ page isELIgnored="false" %> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" 
   "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Bootstrap 101 Template</title>

    <!-- Bootstrap -->
    <link href="./vender/bootstrap-3.3.7-dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" type="text/css" href="./css/default.css">
  </head>
  <body>
    <nav class="navbar navbar-default">
      <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/MVCblog/App"><span class="sr-only">(current)</span>K.K.</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav">
            <li><a href="post.jsp">Posts <span class="sr-only"></span></a></li>
            <li><a href="about.jsp">About <span class="sr-only"></span></a></li>
            <li><a href="contact.jsp">Contact <span class="sr-only"></span></a></li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
			<c:choose>
			 	<c:when test="${user==null }">
			 	 <li><a href="login.jsp">Login</a></li>
			 	</c:when>
			<c:otherwise>
				<li><span class="sr-only">(current)</span>
				<a href="App?action=profile">K.K</a>
				</li>
			</c:otherwise>
			</c:choose>
          </ul>
        </div><!-- /.navbar-collapse -->
      </div><!-- /.container-fluid -->
    </nav>
    <div class="container">
      <div class="row">
        <div class="col-md-3 profile ">
          <aside>
            <img src="images/cat.jpg" alt="a cat image" class="img-circle">
            <h4>KAMON</h4>
            <p>Software Engineer</p>
            <a class="twitter-follow-button"
            href="https://twitter.com/TwitterDev"
            data-show-count="true" data-lanf="en">
          Follow @TwitterDev</a>
            <hr>
            <p>Share This Page:</p>
            <ul class="clearfix">
              <li><img src="images/social_icons/twitter.png"></li>
              <li><img src="images/social_icons/facebook.png"></li>
              <li><img src="images/social_icons/linkedin.png"></li>
              <li><img src="images/social_icons/googleplus.png"></li>
              <li><img src="images/social_icons/pinterest.png"></li>
              <li><img src="images/social_icons/email.png"></li>
            </ul>
          </aside>
        </div>
        <div class="col-md-8 ">
          <h1>Profile</h1>
          <article>
        <c:forEach items="${listOfArticles}" var="article">
             <div class="row">
                <div class="col-md-9">
                <a href="ArticleServlet?articleId=<c:out value="${article.articlesId}" />"><c:out value="${article.title}" />
                 </a>
                </div>
                <div class="col-md-3 date"><c:out value="${article.posted}"/></div>
              </div>
		</c:forEach>
                
        
          </article>

        </div>
      </div>
    </div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="./vender/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
    <script>window.twttr = (function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0],
        t = window.twttr || {};
      if (d.getElementById(id)) return t;
      js = d.createElement(s);
      js.id = id;
      js.src = "https://platform.twitter.com/widgets.js";
      fjs.parentNode.insertBefore(js, fjs);

      t._e = [];
      t.ready = function(f) {
        t._e.push(f);
      };

      return t;
    }(document, "script", "twitter-wjs"));</script>
  </body>
</html>

















