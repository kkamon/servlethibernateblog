package com.MVCblog.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.MVCblog.model.Article;
import com.MVCblog.hibernate.util.HibernateUtil;

public class ArticleDao {

	public void addArticle(Article article){
		Transaction trans = null;
		Session session = HibernateUtil.getSessionFactory().openSession();
		try{
			trans = session.beginTransaction();
			session.save(article);
			session.getTransaction().commit();
		}catch (RuntimeException e) {
			if(trans !=null){
				trans.rollback();
			}
			e.printStackTrace();
		}finally {
			session.flush();
			session.close();
			
		}
	}
	
	public void deleteArticle(int articleId){
		Transaction trans = null;
		Session session = HibernateUtil.getSessionFactory().openSession();
		try{
			trans = session.beginTransaction();
			//get article from DB
			Article article = (Article) session.load(Article.class, new Integer(articleId));
			session.delete(article);
			session.getTransaction().commit();
		}catch (RuntimeException e) {
			if(trans !=null){
				trans.rollback();
			}
			e.printStackTrace();
		}finally {
			session.flush();
			session.close();
		}
	}
	
	public void updateArticle(Article article){
		Transaction trans = null;
		Session session = HibernateUtil.getSessionFactory().openSession();
		try{
			trans = session.beginTransaction();
			session.update(article);
			session.getTransaction().commit();
		}catch(RuntimeException e){
			if(trans !=null){
				trans.rollback();
			}
			e.printStackTrace();
		}finally {
			session.flush();
			session.clear();
		}
	}
	
	public List<Article> getAllArticle(){
		List<Article> articles = new ArrayList<Article>();
		Transaction trans = null;
		Session session = HibernateUtil.getSessionFactory().openSession();
		try{
			trans = session.beginTransaction();
											//article.hbm.xml
			articles = session.createQuery("from Article").list();
		}catch (RuntimeException e) {
			e.printStackTrace();
			System.out.println("something wrong in getAllArticleDao");
		}finally {
			session.flush();
			session.clear();
		}
		return articles;
	}
	
	public Article getArticleById(int articlesId){
		Article article = null;
		
		Session session = HibernateUtil.getSessionFactory().openSession();
		try{
			article = (Article)session.get(Article.class,articlesId);
		}catch (RuntimeException e) {
			e.printStackTrace();
		}finally {
			session.flush();
			session.clear();
		}
		System.out.println("This is article title"+article.getTitle());
		return article;
	}
	public List<Article> getArticleByUsername(String username){
		List<Article> listOfArticles = null;
		Session session = HibernateUtil.getSessionFactory().openSession();
		try{
			String sQuery = "FROM Article a WHERE a.username=:username";
			Query hQuery = session.createQuery(sQuery);
			hQuery.setParameter("username", username);
			listOfArticles = hQuery.list();
		}catch(RuntimeException e){
			e.printStackTrace();
		}finally{
			session.flush();
			session.clear();
		}
		return listOfArticles;
		
	}
	
}
