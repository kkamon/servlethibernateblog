package com.MVCblog.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.MVCblog.model.User;
import com.MVCblog.hibernate.util.HibernateUtil;

public class UserDao {
	
	//Add new user
	public void addUser(User user){
		Transaction trans = null;
		Session session = HibernateUtil.getSessionFactory().openSession();
		try{
			trans = session.beginTransaction();
			session.save(user);
			session.getTransaction().commit();
		}catch (RuntimeException e) {
			if(trans !=null){
				trans.rollback();
			}
			e.printStackTrace();
		}finally{
			session.flush();
			session.close();
		}
	}
	public void deleteUser(int userid){
		Transaction trans = null;
		Session session = HibernateUtil.getSessionFactory().openSession();
		try{
			trans = session.beginTransaction();
			User user = (User) session.load(User.class, new Integer(userid));
			session.delete(user);
			session.getTransaction().commit();
		}catch (RuntimeException e) {
			if(trans !=null){
				trans.rollback();
			}
			e.printStackTrace();
		}finally {
			session.flush();
			session.close();
		}
	}
	
	public void updateUser(User user){
		Transaction trans = null;
		Session session = HibernateUtil.getSessionFactory().openSession();
		try{
			trans= session.beginTransaction();
			session.update(user);
			session.getTransaction().commit();
		}catch (RuntimeException e) {
			if(trans != null){
				trans.rollback();
			}
			e.printStackTrace();
		}finally {
			session.flush();
			session.clear();
		}
	}
	public List<User> getAllUser(){
		List<User> users = new ArrayList<User>();
		Transaction trans = null;
		Session session = HibernateUtil.getSessionFactory().openSession();
		try{
			trans = session.beginTransaction();
			users = session.createQuery("from User").list();
		}catch (RuntimeException e) {
			e.printStackTrace();
			System.out.println("Something wrong in getAllUser UserDao");
		}finally {
			session.flush();
			session.clear();
		}
		return users;
	}
	
	public User getUserByUsername(String username){
		User user = null;
		Transaction trans = null;
		Session session = HibernateUtil.getSessionFactory().openSession();
		try{
			trans = session.beginTransaction();
			String queryString = "from User where username = :username";
			Query query = session.createQuery(queryString);
			query.setParameter("username", username);
			user = (User)query.uniqueResult();
		}catch (RuntimeException e) {
			e.printStackTrace();
		}finally {
			session.flush();
			session.clear();
		}
		
		return user;
	}
	public User getUserById(int userId){
		User user =null;
		
		Session session = HibernateUtil.getSessionFactory().openSession();
		try{
			user = (User)session.get(User.class, userId);
		}catch(RuntimeException e){
			e.printStackTrace();
		}finally{
			session.flush();
			session.clear();
		}
		return user;
	}
	
	public boolean authenticateUser(String username, String password){
		User user = getUserByUsername(username);
		if(user!=null &&user.getUsername().equals(username) && user.getPassword().equals(password)){
			return true;
		}else {return false;}
	}
}
