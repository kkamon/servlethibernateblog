package com.MVCblog.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.MVCblog.dao.ArticleDao;
import com.MVCblog.model.Article;
import com.MVCblog.model.User;

//import jdk.nashorn.internal.ir.RuntimeNode.Request;

/**
 * Servlet implementation class App
 */
public class App extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public App() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String action = (String) request.getParameter("action");
		System.out.println("action = "+action);
		
		if(action==null){
			ArticleDao articleDao = new ArticleDao();
			List<Article> listOfArticles = articleDao.getAllArticle();
			request.setAttribute("listOfArticles", listOfArticles);
			
			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/home.jsp");
			dispatcher.forward(request, response);
		}else if(action.equals("profile")){
			profilePage(request,response);
			System.out.println("execute profilePage method");
		}
		
	}

	private void profilePage(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ArticleDao articleDao = new ArticleDao();
		HttpSession session = request.getSession();
		User user = (User)session.getAttribute("user");
		String username = user.getUsername();
		List<Article> listOfArticles = articleDao.getArticleByUsername(username);
		request.setAttribute("listOfArticles", listOfArticles);
		for(Article articls:listOfArticles){
			System.out.println(articls.getTitle());
		}
		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/profile.jsp");
		dispatcher.forward(request, response);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
