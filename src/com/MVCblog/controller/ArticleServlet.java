package com.MVCblog.controller;

import java.io.IOException;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.MVCblog.dao.ArticleDao;
import com.MVCblog.model.Article;
/**
 * Servlet implementation class Article
 */
public class ArticleServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		ArticleDao articleDao = new ArticleDao();
		String stringId= (String) request.getParameter("articleId");
		System.out.println("String ID "+stringId);
		int articleId = Integer.parseInt(stringId);		
			Article article = articleDao.getArticleById(articleId);
			request.setAttribute("article", article);
		
		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/readArticle.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String author = request.getParameter("author");
		System.out.println(author);
		String articlesTitle = request.getParameter("articlesTitle");
		System.out.println(articlesTitle);
		String articlesText = request.getParameter("articlesText");
		System.out.println(articlesText);
		String Stringdate = request.getParameter("date");
		System.out.println(Stringdate);
		
		Date sqlDate = toSqlDate(Stringdate);
		System.out.println(sqlDate);
		Article article = new Article(articlesText, articlesTitle, sqlDate,author);
		ArticleDao articleDao = new ArticleDao();
		articleDao.addArticle(article);
		
		List<Article> listOfArticles = articleDao.getAllArticle();
		request.setAttribute("listOfArticles", listOfArticles);
		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/home.jsp");
		dispatcher.forward(request, response);
	}
	public Date toSqlDate(String date){
		
		   String startDate = date;
		   SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
		   java.util.Date utilDate = null;
		   java.sql.Date sqlStartDate;
		try {
			utilDate = sdf1.parse(startDate);
		} catch (ParseException e) {
			System.out.println("problem with date toSqlDate()");
			e.printStackTrace();
		}
		   sqlStartDate = new Date(utilDate.getTime()); 

		return sqlStartDate;
	}
	protected void readArticle(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {       
		ArticleDao articleDao = new ArticleDao();
		String stringId= (String) request.getParameter("articleId");
		System.out.println("String ID "+stringId);
		int articleId = Integer.parseInt(stringId);		
			Article article = articleDao.getArticleById(articleId);
			request.setAttribute("article", article);
		
		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/readArticle.jsp");
		dispatcher.forward(request, response);
	}
	protected void displayHome(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
	}

}
