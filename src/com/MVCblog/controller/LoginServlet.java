package com.MVCblog.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.MVCblog.dao.ArticleDao;
import com.MVCblog.dao.UserDao;
import com.MVCblog.model.Article;
import com.MVCblog.model.User;

/**
 * Servlet implementation class LoginServlet
 */
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 HttpSession session = request.getSession();
		 if(session!=null){
		 session.invalidate();
		 }
		 List<Article> listOfArticles = new ArrayList<Article>();
		 ArticleDao articleDao = new ArticleDao();
		 listOfArticles = articleDao.getAllArticle();
		 request.setAttribute("listOfArticles", listOfArticles);
		 for(Article article:listOfArticles){
				System.out.println(article.getTitle());
			}
		 RequestDispatcher dispatcher  = getServletContext().getRequestDispatcher("/home.jsp");
			dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String username = request.getParameter("username");
		System.out.println(username);
		String password = request.getParameter("password");
		System.out.println(password);
		UserDao userDao = new UserDao();
		Boolean result = userDao.authenticateUser(username, password);
		User user = userDao.getUserByUsername(username);
		if(result==true){
			request.getSession().setAttribute("user", user);
			System.out.println(user.getFirstName());
			request.setAttribute("user", user);
			
		}
		else{
			response.sendRedirect("error.jsp");
		}
		ArticleDao articleDao = new ArticleDao();
		List<Article> listOfArticles = articleDao.getAllArticle();
		request.setAttribute("listOfArticles", listOfArticles);
		for(int i=1;i<listOfArticles.size();i++){
			System.out.println(i);
		}
		RequestDispatcher dispatcher  = getServletContext().getRequestDispatcher("/home.jsp");
		dispatcher.forward(request, response);
	}

}
