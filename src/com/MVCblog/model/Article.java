package com.MVCblog.model;

import java.sql.Date;

import java.io.Serializable;/**
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="articles")*/
public class Article implements Serializable{
	//@Id @GeneratedValue
	private int articlesId;
	private String articlesText;
	private String title;
	private Date posted;
	private String username;
	
	public Article(){
		
	}
	
	public Article(String articlesText, String title, Date posted, String username){

		this.articlesText = articlesText;
		this.title = title;
		this.posted = posted;
		this.username = username;
	}

	public int getArticlesId() {
		return articlesId;
	}

	public void setArticlesId(int articlesId) {
		this.articlesId = articlesId;
	}

	public String getArticlesText() {
		return articlesText;
	}

	public void setArticlesText(String articlesText) {
		this.articlesText = articlesText;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Date getPosted() {
		return posted;
	}

	public void setPosted(Date posted) {
		this.posted = posted;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	
}
